//
//  ViewController.swift
//  SpotifyMiniPlayer
//
//  Created by Carl Upton on 08/06/2017.
//  Copyright © 2017 Carl Upton. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    var spotifyScript: SpotifyScriptProtocol!;

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true;

        SpotifyScript.load();
        spotifyScript = SpotifyScript.assign() as! SpotifyScriptProtocol;
    }
    
    override func viewWillAppear() {
        self.view.layer?.backgroundColor = NSColor.black.cgColor;
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func playPauseMusic(_ sender: Any) {
        spotifyScript.playPause();
    }

    @IBAction func prevTrack(_ sender: Any) {
        spotifyScript.prevTrack();
    }
    
    @IBAction func nextTrack(_ sender: Any) {
        spotifyScript.nextTrack();
    }
}


