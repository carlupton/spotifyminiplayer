//
//  SpotifyScriptProtocol.swift
//  SpotifyMiniPlayer
//
//  Created by Carl Upton on 24/06/2017.
//  Copyright © 2017 Carl Upton. All rights reserved.
//

import Foundation

@objc(NSObject) protocol SpotifyScriptProtocol {
    func playPause();
    func prevTrack();
    func nextTrack();
}
