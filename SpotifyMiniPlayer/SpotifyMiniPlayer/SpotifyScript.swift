//
//  SpotifyScript.swift
//  SpotifyMiniPlayer
//
//  Created by Carl Upton on 24/06/2017.
//  Copyright © 2017 Carl Upton. All rights reserved.
//

import Foundation
import AppleScriptObjC

class SpotifyScript {
    
    static func load() {
        Bundle.main.loadAppleScriptObjectiveCScripts();
    }
    
    static func assign() -> AnyObject {
        let ScriptObj = NSClassFromString("SpotifyScriptsObject");
        let obj = ScriptObj?.alloc();
        return obj as AnyObject;
    }
}
