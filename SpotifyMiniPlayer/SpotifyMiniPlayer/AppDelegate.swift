//
//  AppDelegate.swift
//  SpotifyMiniPlayer
//
//  Created by Carl Upton on 08/06/2017.
//  Copyright © 2017 Carl Upton. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

