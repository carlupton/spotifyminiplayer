script SpotifyScriptsObject
    property parent: class "NSObject"
    
    on playPause()
        tell application "Spotify"
            playpause
        end tell
    end playPause
    
    on prevTrack()
        tell application "Spotify"
            previous track
        end tell
    end prevTrack
    
    on nextTrack()
        tell application "Spotify"
            next track
        end tell
    end nextTrack
    
end script
